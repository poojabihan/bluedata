//
//  HistoryModel.swift
//  BlueData
//
//  Created by kdstudio on 27/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class HistoryModel: NSObject {

    var strId = ""
    var strFormId = ""
    var strMachineId = ""
    var strMachineName = ""
    var strUserId = ""
    var strUserName = ""
    var strOperationDate = ""
    var objArrDirective = [DirectiveModel]()
    
}
