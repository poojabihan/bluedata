//
//  UserModel.swift
//  BlueData
//
//  Created by kdstudio on 25/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    static let sharedInstance = UserModel()

    var name = ""
    var department = ""
    var email = ""
    var user_id = ""
    var departmentId = ""
    var office = ""
    var officeId = ""
    var token = ""
    var role = ""
    var organisationID = ""
    var organisation = ""
    var organisationLogo = ""
    var fName = ""
    var lName = ""
    var phoneNo = ""
    var profileImage = ""

}
