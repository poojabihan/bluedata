//
//  DirectiveModel.swift
//  BlueData
//
//  Created by kdstudio on 10/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class DirectiveModel: NSObject {

    var strQuestionId = ""
    var strQuestion = ""
    var strAnswer = ""
    var arrOptions = [String]()
    var arrAllOptions = [OptionsModel]()
    var strInputTypeId = ""
    var arrSelectedOptions = [OptionsModel]()
    var strFormId = ""
    var strImage = UIImage()
    var strImageUrl = ""
}
