//
//  OptionsModel.swift
//  BlueData
//
//  Created by kdstudio on 10/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class OptionsModel: NSObject {

    var strOptionId = ""
    var strOptionValue = ""
    var isSelected = false
}
