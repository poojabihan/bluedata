//
//  MachineModel.swift
//  BlueData
//
//  Created by kdstudio on 25/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class MachineModel: NSObject {

    var strMachineUniqueID = ""
    var strMachineID = ""
    var strMachineName = ""
    var strOfficeID = ""
    var strOfficeName = ""
    var arrInductorIDs = [String]()
    var arrInductorNames = [String]()
    var strInductorsName = ""
    var strOrganisationID = ""
    var strOrganisationName = ""
    var strTrainedUserId = ""
    var strMachineStatus = ""
    var strQrCode = ""
    var strLatitude = ""
    var strLongitude = ""
    var arrMachineImages = [String]()
    var arrTrainedUsersIDs = [String]()
    var arrTrainedUsersNames = [String]()

}
