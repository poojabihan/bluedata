//
//  AppDelegate.swift
//  BlueData
//
//  Created by kdstudio on 11/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
let kConstantObj = kConstant()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        UINavigationBar.appearance().barTintColor = UIColor.init(red: 0/255.0, green: 143.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        let auth = UserModel.sharedInstance
        
        let defaults = UserDefaults.standard
        if defaults.value(forKey: "token") != nil {
            if auth.token == "" {
                setLoginData()
            }
            let mainVcIntial = kConstantObj.SetIntialMainViewController("ScanViewController")
            self.window?.rootViewController = mainVcIntial
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: - Custom Methods
    func setLoginData() {
        
        let defaults = UserDefaults.standard
        let auth = UserModel.sharedInstance
        auth.name = defaults.value(forKey: "name") as! String
        auth.department = defaults.value(forKey: "department") as! String
        auth.email = defaults.value(forKey: "email") as! String
        auth.user_id = defaults.value(forKey: "user_id") as! String
        auth.departmentId = defaults.value(forKey: "departmentId") as! String
        auth.office = defaults.value(forKey: "office") as! String
        auth.officeId = defaults.value(forKey: "officeId") as! String
        auth.token = defaults.value(forKey: "token") as! String
        auth.role = defaults.value(forKey: "role") as! String
        auth.organisationID = defaults.value(forKey: "organisationId") as! String
        auth.organisation = defaults.value(forKey: "organisation") as! String
        auth.organisationLogo = defaults.value(forKey: "organisationLogo") as! String
        auth.profileImage = defaults.value(forKey: "profileImage") as! String

    }
}

