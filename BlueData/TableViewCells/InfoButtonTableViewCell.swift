//
//  InfoButtonTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class InfoButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAttachment: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
