//
//  CommentTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var shadowView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
