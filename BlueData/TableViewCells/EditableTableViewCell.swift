//
//  EditableTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 30/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class EditableTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtSubHeader: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
