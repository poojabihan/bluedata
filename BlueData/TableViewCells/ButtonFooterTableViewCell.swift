//
//  ButtonFooterTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class ButtonFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnMachineDirective: UIButton!
    @IBOutlet weak var btnUserDirective: UIButton!
    @IBOutlet weak var btnAddDoc: UIButton!
    @IBOutlet weak var btnHistory: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
