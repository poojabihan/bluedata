//
//  UploadAttachmentTableViewCell.swift
//  BlueData
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class UploadAttachmentTableViewCell: UITableViewCell {

    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
