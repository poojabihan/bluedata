
//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    let aData : NSArray = ["ProfileMain","Profile","Scan","History","Logout"]
    
    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.reloadTableView(_:)), name: NSNotification.Name(rawValue: "RefreshSideBar"), object: nil)
        
        
    }
    
    //MARK: - Custom Methods
    @objc func reloadTableView(_ notification:Notification) {
        
        self.tableView.reloadData()
    }

    func clearUserDefaults() {
        
        let auth = UserModel.sharedInstance
        auth.name = ""
        auth.department = ""
        auth.email = ""
        auth.user_id = ""
        auth.departmentId = ""
        auth.office = ""
        auth.officeId = ""
        auth.token = ""
        auth.role = ""
        auth.organisationID = ""
        auth.organisation = ""
        auth.organisationLogo = ""
        auth.fName = ""
        auth.lName = ""
        auth.phoneNo = ""
        auth.profileImage = ""

        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "name")
        defaults.removeObject(forKey: "department")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "user_id")
        defaults.removeObject(forKey: "departmentId")
        defaults.removeObject(forKey: "office")
        defaults.removeObject(forKey: "officeId")
        defaults.removeObject(forKey: "token")
        defaults.removeObject(forKey: "role")
        defaults.removeObject(forKey: "organisationId")
        defaults.removeObject(forKey: "organisation")
        defaults.removeObject(forKey: "organisationLogo")
        defaults.removeObject(forKey: "profileImage")
        
    }
    
    //MARK: WebService Methods
    func callWebServiceLogout() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["":""]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.clearUserDefaults()
                    let mainVcIntial = kConstantObj.SetIntialMainViewController("ViewController")
                    self.appDelegate.window?.rootViewController = mainVcIntial
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    //MARK: - UITableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let aCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            aCell.lblUsername.text = UserModel.sharedInstance.name
            aCell.lblEmail.text = UserModel.sharedInstance.email
            aCell.imgProfile.kf.setImage(with: URL(string: UserModel.sharedInstance.profileImage), placeholder: #imageLiteral(resourceName: "user_default"))
            aCell.selectionStyle = .none;
            return aCell
        }
        else{
            let aCell = tableView.dequeueReusableCell(withIdentifier: "kCell", for: indexPath)
            let aLabel : UILabel = aCell.viewWithTag(10) as! UILabel
            aLabel.text = aData[indexPath.row] as? String
            return aCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.row {
        case 0,1:
            kConstantObj.SetIntialMainViewController("ProfileViewController")
       case 2:
            kConstantObj.SetIntialMainViewController("ScanViewController")
        case 3:
            kConstantObj.SetIntialMainViewController("HistoryViewController")
        default:
            let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.callWebServiceLogout()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 300
        }
        else {
            return 60
        }
    }
}
