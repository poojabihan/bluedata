//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

//OLD PROD URL
//let kBaseURL = "http://production.chetaru.co.uk/bluedata/api/"

//OLD LIVE URL
//let kBaseURL = "http://bluedata.chetaru.co.uk/api/"

//NEW LIVE URL
let kBaseURL = "https://bluedata.chetaru.co.uk/api/"

//NEW PROD URL
//let kBaseURL = "https://bluedatademo.chetaru.co.uk/api/"

class NetworkConstant: NSObject {
    
    struct Auth {
        static let login = "login"
        static let logout = "logout"
        static let forgotPassword = "forgotPassword"
        static let userProfile = "getUserProfile"
        static let changePassword = "updatePasswordWithCurrentPassword"
        static let updateProfile = "updateUserProfile"
    }
    
    struct Machine {
        static let machineDetails = "machineDetail"
        static let machineDirectiveDetails = "getMachineDirective"
        static let userDirectiveDetails = "getUserDirective"
        static let userDirectiveFile = "addUserDirectiveFile"
        static let userDirectiveAnswers = "addUserDirectiveAnswers"
        static let getMachineOperationHistory = "getMachineOperationUserList"
        static let getAllOperationHistory = "opertaionHistory"
    }
    
}



