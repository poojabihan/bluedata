//
//  ParseMachineDetails.swift
//  BlueData
//
//  Created by kdstudio on 25/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

class ParseMachineDetails: NSObject {

    class func parseMachineDetails(response : JSON, completionHandler: @escaping (MachineModel) -> Void) {
        
        let model = MachineModel()
        
        model.strMachineUniqueID = response["data"]["id"].stringValue
        model.strMachineID = response["data"]["machineId"].stringValue
        model.strMachineName = response["data"]["machine"].stringValue
        model.strOfficeID = response["data"]["officeId"].stringValue
        model.strOfficeName = response["data"]["officeName"].stringValue
        model.strOrganisationName = response["data"]["orgName"].stringValue

        for value in response["data"]["inductors"].arrayValue {
            model.arrInductorIDs.append(value["id"].stringValue)
            model.arrInductorNames.append(value["inductorName"].stringValue)
        }
        model.strInductorsName = model.arrInductorNames.joined(separator: ", ")

        for value in response["data"]["trainedUsers"].arrayValue {
            model.arrTrainedUsersIDs.append(value["id"].stringValue)
            model.arrTrainedUsersNames.append(value["trainedUserName"].stringValue)
        }
        
        model.strOrganisationID = response["data"]["orgId"].stringValue
        model.strTrainedUserId = response["data"]["trainedUserId"].stringValue
        model.strMachineStatus = response["data"]["machineStatus"].stringValue
        model.strQrCode = response["data"]["qrCode"].stringValue
        model.strLatitude = response["data"]["latitude"].stringValue
        model.strLongitude = response["data"]["longitude"].stringValue
        
        for value in response["data"]["machineImages"].arrayValue {
            model.arrMachineImages.append(value["image"].stringValue.replacingOccurrences(of: "\\", with: "//"))
        }
        
        completionHandler(model)
    }
    
    class func parseMachineDirectives(response: JSON, fromHistory: Bool, completionHandler: @escaping ([DirectiveModel]) -> Void) {
        
        var arrModel = [DirectiveModel]()
        
        var arrData = [JSON]()
        
        if fromHistory {
            arrData = response["form"].arrayValue
        }
        else {
            arrData = response["data"].arrayValue
        }
        
        for directive in arrData {
            
            let model = DirectiveModel()
            
            model.strQuestionId = directive["questionId"].stringValue
            model.strQuestion = directive["question"].stringValue
            model.strAnswer = directive["answer"].stringValue
            for value in directive["options"].arrayValue {
                model.arrOptions.append(value.stringValue)
            }
            model.strInputTypeId = directive["inputTypeId"].stringValue

            for value in directive["allOptions"].arrayValue {
                let option = OptionsModel()
                option.strOptionId = value["optionId"].stringValue
                option.strOptionValue = value["optionValue"].stringValue
                model.arrAllOptions.append(option)
            }
            
            if model.strInputTypeId == "4" {
                for value in model.arrAllOptions {
                    if (model.arrOptions.contains(value.strOptionId)) {
                        model.strAnswer = value.strOptionValue
                        break
                    }
                }
            }
            
            arrModel.append(model)

            if model.strInputTypeId != "4" {
                for value in directive["allOptions"].arrayValue {
                    let option = OptionsModel()
                    option.strOptionId = value["optionId"].stringValue
                    option.strOptionValue = value["optionValue"].stringValue
                    
                    let submodel = DirectiveModel()
                    submodel.strQuestionId = option.strOptionId
                    submodel.strQuestion = option.strOptionValue
                    submodel.strAnswer = ""
                    for value in directive["options"].arrayValue {
                        submodel.arrOptions.append(value.stringValue)
                    }
                    submodel.strInputTypeId = directive["inputTypeId"].stringValue
                    arrModel.append(submodel)
                
                }
            }

        }
        
        completionHandler(arrModel)

    }
    
    class func parseUserDirectives(response : JSON, completionHandler: @escaping ([DirectiveModel]) -> Void) {
        
        var arrModel = [DirectiveModel]()
        
        for directive in response["data"].arrayValue {
            
            let model = DirectiveModel()
            
            model.strQuestionId = directive["questionId"].stringValue
            model.strQuestion = directive["question"].stringValue
            model.strInputTypeId = directive["inputTypeId"].stringValue
            model.strFormId = response["formId"].stringValue

            for value in directive["allOptions"].arrayValue {
                let option = OptionsModel()
                option.strOptionId = value["optionId"].stringValue
                option.strOptionValue = value["optionValue"].stringValue
                model.arrAllOptions.append(option)
            }
            
            arrModel.append(model)
            
        }
        
        completionHandler(arrModel)
        
    }
    
    /*class func parseHistoryList(response : JSON, completionHandler: @escaping ([HistoryModel]) -> Void) {
        
        var arrModel = [HistoryModel]()
        
        for directive in response["data"].arrayValue {
            
            let model = HistoryModel()
            
            model.strId = directive["id"].stringValue
            model.strFormId = directive["formId"].stringValue
            model.strMachineId = directive["machineId"].stringValue
            model.strUserId = directive["userId"].stringValue
            model.strUserName = directive["userName"].stringValue
            model.strMachineName = directive["machineName"].stringValue
            model.strOperationDate = directive["operationDate"].stringValue
            
             self.parseMachineDirectives(response: directive, fromHistory: true, completionHandler: { (modelDir) in
                model.objArrDirective = modelDir
            })
            
            arrModel.append(model)
        }
        
        completionHandler(arrModel)
    }*/
    
    class func parseOperationHistoryList(response : JSON, completionHandler: @escaping ([OperationHistoryModel]) -> Void) {
        
        var arrModel = [OperationHistoryModel]()
        
        var arrHistory = [HistoryModel]()
        var arrMachineId = [String]()

        for directive in response["data"].arrayValue {
            
            let model = HistoryModel()
            
            model.strId = directive["id"].stringValue
            model.strFormId = directive["formId"].stringValue
            model.strMachineId = directive["machineId"].stringValue
            model.strUserId = directive["userId"].stringValue
            model.strUserName = directive["userName"].stringValue
            model.strMachineName = directive["machineName"].stringValue
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEE, MMM d, yyyy | HH:mm"
            
            if let date = dateFormatterGet.date(from: directive["operationDate"].stringValue){
                model.strOperationDate = dateFormatterPrint.string(from: date)
            }
            else {
                model.strOperationDate = directive["operationDate"].stringValue
                print("There was an error decoding the string")
            }
            
            self.parseMachineDirectives(response: directive, fromHistory: true, completionHandler: { (modelDir) in
                model.objArrDirective = modelDir
            })
            
            if !arrMachineId.contains(model.strMachineId) {
                arrMachineId.append(model.strMachineId)
            }
            
            arrHistory.append(model)
        }
        
        for value in arrMachineId {
            
            let model = OperationHistoryModel()

            for history in arrHistory {
                if value == history.strMachineId {
                    model.strMachines = history.strMachineName
                    model.arrMachineHistory.append(history)
                }
            }
            
            arrModel.append(model)
        }
        
        completionHandler(arrModel)
    }
    
    class func parseUserDetails(json : JSON, completionHandler: @escaping (UserModel) -> Void) {
        
        let auth = UserModel.sharedInstance
        
        auth.name = json["data"]["name"].stringValue
        auth.department = json["data"]["department"].stringValue
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.departmentId = json["data"]["departmentId"].stringValue
        auth.office = json["data"]["office"].stringValue
        auth.officeId = json["data"]["officeId"].stringValue
        auth.token = json["data"]["token"].stringValue
        auth.role = json["data"]["role"].stringValue
        auth.organisationID = json["data"]["organisationId"].stringValue
        auth.organisation = json["data"]["organisation"].stringValue
        auth.organisationLogo = json["data"]["organisationLogo"].stringValue
        auth.fName = json["data"]["firstName"].stringValue
        auth.lName = json["data"]["lastName"].stringValue
        auth.phoneNo = json["data"]["userContact"].stringValue
        auth.profileImage = json["data"]["profileImage"].stringValue
        
        let defaults = UserDefaults.standard
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.profileImage, forKey: "profileImage")

        completionHandler(auth)
        
    }
}
