//
//  ViewController.swift
//  BlueData
//
//  Created by kdstudio on 11/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import SwiftyJSON
import NVActivityIndicatorView

class ViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var txtForgotEmail: UITextField!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var selectedRole = "3"//3 for user

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib. 
        txtUsername.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    //MARK: Action Methods
    @IBAction func btnLoginAction() {
        if (txtUsername.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Username.")
        }
        else if (txtUsername.text?.isValidEmail())! {
            if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Password.")
            }
            else {
                callWebServiceForLogin()
            }
        }
        else {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid Username.")
        }
    }
    
    @IBAction func btnForgotPasswordAction() {
        showForgotPasswordView()
    }
    
    @IBAction func btnForgotPasswordSubmitAction() {
        
        if txtForgotEmail.text == "" {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email address.")
        }
        else if (txtForgotEmail.text?.isValidEmail())! {
            callWebServiceForForgotPassword()
        }
        else {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email address.")
        }
    }
    
    @IBAction func btnForgotPasswordCancelAction() {
        showLoginView()
    }
    
    //MARK: WebService Methods
    func callWebServiceForLogin() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = ["email":txtUsername.text ?? "","password":txtPassword.text ?? "",
                     "role":selectedRole]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.setLoginData(json: response!)
                    let mainVcIntial = kConstantObj.SetIntialMainViewController("ScanViewController")
                    self.appDelegate.window?.rootViewController = mainVcIntial
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForForgotPassword() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["email":txtForgotEmail.text ?? ""]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.forgotPassword, param: param, withHeader: false) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    let msg = response!["message"].stringValue
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: msg)
                    self.showLoginView()                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    func setLoginData(json: JSON) {
        
        let auth = UserModel.sharedInstance
        auth.name = json["data"]["name"].stringValue
        auth.department = json["data"]["department"].stringValue
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.departmentId = json["data"]["departmentId"].stringValue
        auth.office = json["data"]["office"].stringValue
        auth.officeId = json["data"]["officeId"].stringValue
        auth.token = json["data"]["token"].stringValue
        auth.role = json["data"]["role"].stringValue
        auth.organisationID = json["data"]["organisationId"].stringValue
        auth.organisation = json["data"]["organisation"].stringValue
        auth.organisationLogo = json["data"]["organisationLogo"].stringValue
        auth.profileImage = json["data"]["profileImage"].stringValue

        let defaults = UserDefaults.standard
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.department, forKey: "department")
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.user_id, forKey: "user_id")
        defaults.set(auth.department, forKey: "departmentId")
        defaults.set(auth.office, forKey: "office")
        defaults.set(auth.officeId, forKey: "officeId")
        defaults.set(auth.token, forKey: "token")
        defaults.set(auth.role, forKey: "role")
        defaults.set(auth.organisationID, forKey: "organisationId")
        defaults.set(auth.organisation, forKey: "organisation")
        defaults.set(auth.organisationLogo, forKey: "organisationLogo")
        defaults.set(auth.profileImage, forKey: "profileImage")

    }
    
    func showForgotPasswordView(){
        UIView.transition(with: forgotPasswordView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.forgotPasswordView.isHidden = false
            self.loginView.isHidden = true
        })
    }
    
    func showLoginView(){
        UIView.transition(with: loginView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.forgotPasswordView.isHidden = true
            self.loginView.isHidden = false
        })
    }
}
