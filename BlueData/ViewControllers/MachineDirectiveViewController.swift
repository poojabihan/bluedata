//
//  MachineDirectiveViewController.swift
//  BlueData
//
//  Created by kdstudio on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import Kingfisher

class MachineDirectiveViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    //MARK: IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tblMachineDirectives: UITableView!

    //MARK: Variables
    let panel = JKNotificationPanel()
    var arrDirective = [DirectiveModel]()
    var machineID = ""
    var isFromHistory = false

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shadowView.dropShadow()
        tblMachineDirectives.estimatedRowHeight = 65.0
        tblMachineDirectives.tableFooterView = UIView()
        
        if !isFromHistory {
            callWebServiceForMachineDirectiveDetails()
        }
        else {
            self.title = "USER DIRECTIVE"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Action Methods
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func btnAttachmentAction(_sender: UIButton) {
        
        if arrDirective[_sender.tag].strAnswer == "" {
            return
        }
        
        UIApplication.shared.open(URL(string : arrDirective[_sender.tag].strAnswer)!, options: [:], completionHandler: { (status) in
        })
    }
    
    //MARK: UITableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDirective.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch arrDirective[indexPath.row].strInputTypeId {
        case "1","4","5","8":
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrDirective[indexPath.row].strQuestion
            cell.lblSubTitle.text = arrDirective[indexPath.row].strAnswer
            return cell
        case "2","3":
            if arrDirective[indexPath.row].arrAllOptions.count != 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellQue", for: indexPath) as! CheckboxTableViewCell
                cell.lblQuestion.text = arrDirective[indexPath.row].strQuestion
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellOption", for: indexPath) as! CheckboxTableViewCell
                cell.lblOption.text = arrDirective[indexPath.row].strQuestion
                if arrDirective[indexPath.row].arrOptions.contains(arrDirective[indexPath.row].strQuestionId) {
                    arrDirective[indexPath.row].strInputTypeId == "2" ? cell.btnOption.setImage(#imageLiteral(resourceName: "Check"), for: .normal) : cell.btnOption.setImage(#imageLiteral(resourceName: "Radio_Check"), for: .normal)
                }
                else {
                    arrDirective[indexPath.row].strInputTypeId == "2" ? cell.btnOption.setImage(#imageLiteral(resourceName: "Uncheck"), for: .normal) : cell.btnOption.setImage(#imageLiteral(resourceName: "Radio_Uncheck"), for: .normal)
                }
                return cell
            }
        case "6":
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoButtonTableViewCell", for: indexPath) as! InfoButtonTableViewCell
            cell.lblTitle.text = arrDirective[indexPath.row].strQuestion
            cell.btnAttachment.tag = indexPath.row
            cell.btnAttachment.addTarget(self, action: #selector(btnAttachmentAction(_sender:)), for: .touchUpInside)
            cell.btnAttachment.setImage(#imageLiteral(resourceName: "attach"), for: .normal)
            arrDirective[indexPath.row].strAnswer == "" ? cell.btnAttachment.setTitle(" No file selected", for: .normal) : cell.btnAttachment.setTitle(" " + (URL.init(string: arrDirective[indexPath.row].strAnswer)?.lastPathComponent.capitalized)! , for: .normal)
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrDirective[indexPath.row].strQuestion
            cell.lblSubTitle.text = arrDirective[indexPath.row].strAnswer
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension

/*        switch arrDirective[indexPath.row].strInputTypeId {
        case "6":
            return 65
        case "2","3":
            return 40
        default:
            return UITableViewAutomaticDimension
        }*/
    }

    //MARK: WebService Methods
    func callWebServiceForMachineDirectiveDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["machineId" : machineID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Machine.machineDirectiveDetails, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ParseMachineDetails.parseMachineDirectives(response: response!, fromHistory: false, completionHandler: { (arrModel) in
                        self.arrDirective = arrModel
                    })
                    
                    self.tblMachineDirectives.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
