//
//  UserDirectiveViewController.swift
//  BlueData
//
//  Created by kdstudio on 13/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import Photos
import MobileCoreServices

class UserDirectiveViewController: UIViewController, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    

    //MARK: IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tblUserDirective: UITableView!

    //Textfield view
    @IBOutlet weak var btnNextTextField: UIButton!
    @IBOutlet weak var btnPrevTextField: UIButton!
    @IBOutlet weak var txtTextField: UITextField!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var lblTextFieldTitle: UILabel!

    //Comment view
    @IBOutlet weak var btnNextComment: UIButton!
    @IBOutlet weak var btnPrevComment: UIButton!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var lblcommentTitle: UILabel!

    //Upload view
    @IBOutlet weak var lblUploadImageTitle: UILabel!
    @IBOutlet weak var btnNextUploadFile: UIButton!
    @IBOutlet weak var btnPrevUploadFile: UIButton!
    @IBOutlet weak var uploadView: UIView!
    @IBOutlet weak var imgViewUpload: UIImageView!

    //Date view
    @IBOutlet weak var btnNextDatePicker: UIButton!
    @IBOutlet weak var btnPrevDatePicker: UIButton!
    @IBOutlet weak var datePicker = UIDatePicker()
    @IBOutlet weak var txtDatePicker: UITextField!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var lblDateTitle: UILabel!

    //DropDown view
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var dropDown: HADropDown!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    @IBOutlet weak var btnNextDropdown: UIButton!
    @IBOutlet weak var btnPrevDropdown: UIButton!

    //MARK: Variables
    var currentIndex = 0
    var imagePicker = UIImagePickerController()
    var machineID = ""
    let panel = JKNotificationPanel()
    var arrDirective = [DirectiveModel]()
    var docUrl: URL!
    var isNewImage = false

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shadowView.dropShadow()
        commentView.dropShadow()
        uploadView.dropShadow()
        imagePicker.delegate = self
        
        tblUserDirective.estimatedRowHeight = 40.0
        tblUserDirective.tableFooterView = UIView()
        tblUserDirective.tableHeaderView = nil
        callWebServiceForUserDirectiveDetails()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    
    //MARK: Custom Methods
    @objc func openiCloud(){

        let importMenu = UIDocumentPickerViewController (documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
//        importMenu.modalPresentationStyle = .formSheet
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
        }
        
       self.present(importMenu, animated: true, completion:nil)

    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .white
        })
    }
    
    func setUpTextFieldView() {
        tblUserDirective.tableHeaderView = textFieldView
        txtTextField.text = arrDirective[currentIndex].strAnswer
        lblTextFieldTitle.text = arrDirective[currentIndex].strQuestion
        
        let btnTitle = currentIndex == arrDirective.count - 1 ? "Finish" : "Next"
        changeBtnNameToSave(_sender: btnNextTextField, title: btnTitle)
        
        btnPrevTextField.isHidden = currentIndex == 0 ? true : false
        
        getHeaderHeight(lbl: lblTextFieldTitle, heightToIncrease: 135, txt: arrDirective[currentIndex].strQuestion)
    }
    
    func setUpCommentView() {
        tblUserDirective.tableHeaderView = commentView
        txtComment.text = arrDirective[currentIndex].strAnswer
        lblcommentTitle.text = arrDirective[currentIndex].strQuestion
        
        let btnTitle = currentIndex == arrDirective.count - 1 ? "Finish" : "Next"
        changeBtnNameToSave(_sender: btnNextComment, title: btnTitle)
        
        btnPrevComment.isHidden = currentIndex == 0 ? true : false
        
        getHeaderHeight(lbl: lblcommentTitle, heightToIncrease: 230, txt: arrDirective[currentIndex].strQuestion)
    }
    
    func setUpDatePickerView() {
        tblUserDirective.tableHeaderView = datePickerView
        lblDateTitle.text = arrDirective[currentIndex].strQuestion
        txtDatePicker.text = arrDirective[currentIndex].strAnswer
        
        let btnTitle = currentIndex == arrDirective.count - 1 ? "Finish" : "Next"
        changeBtnNameToSave(_sender: btnNextDatePicker, title: btnTitle)
        
        btnPrevDatePicker.isHidden = currentIndex == 0 ? true : false
        
        getHeaderHeight(lbl: lblDateTitle, heightToIncrease: 290, txt: arrDirective[currentIndex].strQuestion)
    }
    
    func setUpUploadView() {
        tblUserDirective.tableHeaderView = uploadView
        lblUploadImageTitle.text = arrDirective[currentIndex].strQuestion
        
        if arrDirective[currentIndex].strAnswer != ""{
            if arrDirective[currentIndex].strAnswer.contains("png") || arrDirective[currentIndex].strAnswer.contains("jpg") || arrDirective[currentIndex].strAnswer.contains("jpeg") {
                
                imgViewUpload.kf.setImage(with: URL.init(string: arrDirective[currentIndex].strImageUrl), placeholder: #imageLiteral(resourceName: "image_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)

            }
            else {
                imgViewUpload.kf.setImage(with: URL.init(string: arrDirective[currentIndex].strImageUrl), placeholder: #imageLiteral(resourceName: "doc"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
        }
        else {
            imgViewUpload.image = arrDirective[currentIndex].strImage
        }
        
        let btnTitle = currentIndex == arrDirective.count - 1 ? "Finish" : "Next"
        changeBtnNameToSave(_sender: btnNextUploadFile, title: btnTitle)
        
        btnPrevUploadFile.isHidden = currentIndex == 0 ? true : false
        
        getHeaderHeight(lbl: lblUploadImageTitle, heightToIncrease: 180, txt: arrDirective[currentIndex].strQuestion)
    }
    
    func setUpDropdownView() {
        tblUserDirective.tableHeaderView = dropdownView
        lblDropDownTitle.text = arrDirective[currentIndex].strQuestion
        dropDown.delegate = self
        dropDown.items = arrDirective[currentIndex].arrAllOptions
        
        if arrDirective[currentIndex].strAnswer != "" {
            
            for value in arrDirective[currentIndex].arrAllOptions {
                
                if arrDirective[currentIndex].arrOptions[0] == value.strOptionId {
                    dropDown.title = value.strOptionValue
                    dropDown.selectedIndex = Int(arrDirective[currentIndex].strAnswer)!
                    break
                }
            }
        }
        else {
            dropDown.title = "Select"
            dropDown.selectedIndex = -1
        }
        
        let btnTitle = currentIndex == arrDirective.count - 1 ? "Finish" : "Next"
        changeBtnNameToSave(_sender: btnNextUploadFile, title: btnTitle)

        btnPrevUploadFile.isHidden = currentIndex == 0 ? true : false
        
        getHeaderHeight(lbl: lblDropDownTitle, heightToIncrease: 145, txt: arrDirective[currentIndex].strQuestion)
    }
    
    func getHeaderHeight(lbl: UILabel, heightToIncrease: CGFloat, txt: String) {
        if let headerView = tblUserDirective.tableHeaderView {
            var headerFrame = headerView.frame
            //Comparison necessary to avoid infinite loop
            headerFrame.size.height = txt.height(withConstrainedWidth: lbl.frame.size.width, font: lbl.font) + heightToIncrease
            headerView.frame = headerFrame
            tblUserDirective.tableHeaderView = headerView
            tblUserDirective.layoutIfNeeded()
        }
    }
    
    func loadFormView() {
        switch arrDirective[currentIndex].strInputTypeId {
        case "1"://TextBox
            setUpTextFieldView()
            break;
        case "2"://Checkbox
            tblUserDirective.tableHeaderView = nil
        case "3"://Radio
            tblUserDirective.tableHeaderView = nil
        case "4"://Drop Down
            setUpDropdownView()
        case "5"://Date
            setUpDatePickerView()
            break;
        case "6"://File
            setUpUploadView()
            break;
        case "7"://Hidden
            setUpTextFieldView()
            break;
        case "8"://Textarea
            setUpCommentView()
            break;
        default:
            tblUserDirective.tableHeaderView = nil
        }
        tblUserDirective.reloadData()
        
    }
    
    func changeBtnNameToSave(_sender: UIButton, title: String) {
        _sender.setTitle(title, for: .normal)
    }
    
    func getAnswerForForm() -> String{
        switch arrDirective[currentIndex].strInputTypeId {
        case "1"://TextBox
            return txtTextField.text!
        case "2"://Checkbox
            return arrDirective[currentIndex].strAnswer
        case "3"://Radio
            return arrDirective[currentIndex].strAnswer
        case "4"://Drop Down
            return arrDirective[currentIndex].strAnswer
        case "5"://Date
            return txtDatePicker.text!
        case "6"://File
            return arrDirective[currentIndex].strAnswer
        case "7"://Hidden
            return txtTextField.text!
        case "8"://Textarea
            return txtComment.text
        default:
            return arrDirective[currentIndex].strAnswer
        }
    }
    
    @objc func btnCheckBoxClicked(_sender: UIButton){
        
        if self.arrDirective[currentIndex].strInputTypeId == "3" {
            self.arrDirective[currentIndex].strAnswer = self.arrDirective[currentIndex].arrAllOptions[_sender.tag].strOptionId
        }
        else {
            self.arrDirective[currentIndex].arrAllOptions[_sender.tag].isSelected = !self.arrDirective[currentIndex].arrAllOptions[_sender.tag].isSelected
        }
        
        tblUserDirective.reloadData()
    }
    
    func getCheckUncheckImage(index: Int) -> UIImage{
        if self.arrDirective[currentIndex].strInputTypeId == "3" {
            if self.arrDirective[currentIndex].arrAllOptions[index].strOptionId == self.arrDirective[currentIndex].strAnswer {
                return #imageLiteral(resourceName: "Radio_Check")
            }
            else{
                return #imageLiteral(resourceName: "Radio_Uncheck")
            }
        }
        else {
            if self.arrDirective[currentIndex].arrAllOptions[index].isSelected {
                return #imageLiteral(resourceName: "Check")
            }
            else{
                return #imageLiteral(resourceName: "Uncheck")
            }
        }
    }
    
    //MARK: Action Methods
    @IBAction func btnUploadAction(_sender: UIButton) {

        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction(title: "iCloud", style: .default, handler: { _ in
            self.perform(#selector(self.openiCloud), with: nil, afterDelay: 0.5)

//            self.openiCloud()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    

    
    @objc func btnSaveAction(_sender: UIButton) {
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loadNextView() {
        
//        if currentIndex == arrDirective.count - 1 {
//            //save data
//            if (arrDirective[currentIndex].strInputTypeId == "6"){
//                callWebServiceForUploadImage(index: currentIndex)
//            }
//            else {
//                callWebServiceForSaveUserDirectiveDetails()
//            }
//        }
//        else{
            
            if arrDirective[currentIndex].strInputTypeId == "2"{
                
                for value in arrDirective[currentIndex].arrAllOptions {
                    
                    if value.isSelected {
                        arrDirective[currentIndex].arrOptions.removeAll()
                        arrDirective[currentIndex].arrOptions.append(value.strOptionId)
                    }
                }
            }
            else if arrDirective[currentIndex].strInputTypeId == "3"{
                
                if arrDirective[currentIndex].strAnswer != "" {
                    arrDirective[currentIndex].arrOptions.removeAll()
                    arrDirective[currentIndex].arrOptions.append(arrDirective[currentIndex].strAnswer)
                }
            }
            else if (arrDirective[currentIndex].strInputTypeId != "6"){
                arrDirective[currentIndex].strAnswer = getAnswerForForm()
            }
            else if isNewImage{
                callWebServiceForUploadImage(index: currentIndex)
                return
            }
        
            
            if currentIndex == arrDirective.count - 1 {
                callWebServiceForSaveUserDirectiveDetails()
            }
            else {
                currentIndex = currentIndex == arrDirective.count - 1 ? currentIndex : currentIndex + 1
                loadFormView()
            }
            
//        }
    }
    
    @IBAction func loadPrevView() {
        
        currentIndex = currentIndex == 0 ? 0 : currentIndex - 1
        loadFormView()
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE dd MMM yyyy"
            txtDatePicker.text = formatter.string(from: (datePicker?.date)!)
        }
    }
    
    

    //MARK: UIImagePickerController Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                // imageViewPic.contentMode = .scaleToFill
                imgViewUpload.image = pickedImage
                isNewImage = true
            }
            picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: UITableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrDirective.count > 0 {
            if arrDirective[currentIndex].strInputTypeId == "2" || arrDirective[currentIndex].strInputTypeId == "3" {
                return arrDirective[currentIndex].arrAllOptions.count
            }
            else {
                return 0
            }
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellOpt", for: indexPath) as! CheckboxTableViewCell
        
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellQue", for: indexPath) as! CheckboxTableViewCell
            cell.lblQuestion.text = arrDirective[currentIndex].strQuestion
            cell.lblOption.text = arrDirective[currentIndex].arrAllOptions[indexPath.row].strOptionValue
            break
        case arrDirective[currentIndex].arrAllOptions.count - 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellLast", for: indexPath) as! CheckboxTableViewCell
            cell.lblOption.text = arrDirective[currentIndex].arrAllOptions[indexPath.row].strOptionValue
            cell.btnNext.addTarget(self, action: #selector(loadNextView), for: .touchUpInside)
            cell.btnPrev.addTarget(self, action: #selector(loadPrevView), for: .touchUpInside)
            break
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxTableViewCellOpt", for: indexPath) as! CheckboxTableViewCell
            cell.lblOption.text = arrDirective[currentIndex].arrAllOptions[indexPath.row].strOptionValue
            break
        }
        
        cell.btnOption.tag = indexPath.row
        cell.btnOption.addTarget(self, action: #selector(btnCheckBoxClicked(_sender:)), for: .touchUpInside)
        cell.btnOption.setImage(getCheckUncheckImage(index: indexPath.row), for: .normal)
        
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    // MARK: - UIDocumentPickerDelegate Methods
    
    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController {
        if let navigationController = self.navigationController {
            return navigationController
        } else {
            return self
        }
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        docUrl = myURL
        print(myURL)
        
        imgViewUpload.image = UIImage(named:"doc")
        
        isNewImage = true
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }

    
    //MARK: WebService Methods
    func callWebServiceForUploadImage(index: Int) {
        
        if (imgViewUpload.image?.size)!.equalTo(.zero){
            if currentIndex == arrDirective.count - 1 {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please select image")
            }
            return
        }
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var isImageData = true
        
        var imgData = UIImageJPEGRepresentation(imgViewUpload.image!, 1.0)!
        if let imageData = imgViewUpload.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }

        if imgViewUpload.image == #imageLiteral(resourceName: "doc") {
            isImageData = false
        }

        WebServiceHandler.postMultiPartReq(fileParamName: "file", url: kBaseURL + NetworkConstant.Machine.userDirectiveFile, docUrl: docUrl, imageData: isImageData ? imgData : nil, parameters: nil, withHeader: true) { (response, errorMsg) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else {
                
                print(response)
                
                self.arrDirective[index].strAnswer = response!["answer"].stringValue
                self.arrDirective[index].strImage = self.imgViewUpload.image!
                self.arrDirective[index].strImageUrl = response!["fileUrl"].stringValue
                self.isNewImage = false
                
                if (index == self.arrDirective.count - 1) {
                    self.callWebServiceForSaveUserDirectiveDetails()
                }
                else {
                    self.currentIndex = self.currentIndex == self.arrDirective.count - 1 ? self.currentIndex : self.currentIndex + 1
                    self.loadFormView()
                }
                

            }
        }
    }
    
    func callWebServiceForUserDirectiveDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["machineId" : machineID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Machine.userDirectiveDetails, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                
                if response!["status"].boolValue == true {
                    
                    ParseMachineDetails.parseUserDirectives(response: response!, completionHandler: { (arrModel) in
                        self.arrDirective = arrModel
                    })
                    
                    self.tblUserDirective.reloadData()
                    self.loadFormView()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForSaveUserDirectiveDetails() {
        
        var error = false
        
        for value in arrDirective{
            
            if value.strAnswer == "" && value.arrOptions.count == 0 {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter " + value.strQuestion)
                error = true
                break
            }
        }
        
        if error {
            return
        }
        else {
            
            
            var arrAnswers = [[String : Any]]()
            
            for value in arrDirective {
                
                var subDict = [String : Any]()
                subDict["questionId"] = value.strQuestionId
                subDict["inputTypeId"] = value.strInputTypeId
                subDict["answer"] = value.strAnswer
                subDict["option"] = value.arrOptions
                
                arrAnswers.append(subDict)
                
            }
            
            let param = ["machineId" : machineID,
                         "formId" : arrDirective[0].strFormId,
                         "answers" : arrAnswers] as [String : Any]
            
            print("-----", param)
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

            WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Machine.userDirectiveAnswers, param: param, withHeader: true) { (response, errorMsg) in
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
                if response == nil {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
                else{
                    
                    if response!["status"].boolValue == true {
                        
                        let msg = response!["message"].stringValue
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: msg)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserDirectiveViewController: HADropDownDelegate {
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(index)")
        self.arrDirective[currentIndex].arrOptions = [self.arrDirective[currentIndex].arrAllOptions[index].strOptionId]
        self.arrDirective[currentIndex].strAnswer = String(index)
    }
}
