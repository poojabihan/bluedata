//
//  HistoryViewController.swift
//  BlueData
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: IBOutlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var btnLeftNav: UIButton!

    //MARK: Variables
    var machineID = ""
    var ifFromMachine = false
    let panel = JKNotificationPanel()
    var arrOperationHistory = [OperationHistoryModel]()
    var arrSelectedDirective = [DirectiveModel]()

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shadowView.dropShadow()
        tblHistory.tableFooterView = UIView()
        
        if ifFromMachine {
            btnLeftNav.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            btnLeftNav.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        }
        else {
            btnLeftNav.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
            btnLeftNav.addTarget(self, action: #selector(btnSideMenuPressed(_:)), for: .touchUpInside)
        }
        
        callWebServiceForHistory()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setShadow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction Methods
    @objc func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @objc func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrOperationHistory.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOperationHistory[section].arrMachineHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrOperationHistory[indexPath.section].arrMachineHistory[indexPath.row].strMachineName
            cell.lblSubTitle.text = arrOperationHistory[indexPath.section].arrMachineHistory[indexPath.row].strOperationDate
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoDateTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrOperationHistory[indexPath.section].arrMachineHistory[indexPath.row].strOperationDate
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0){
            return 78
        }
        else {
            return 40
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        arrSelectedDirective = arrOperationHistory[indexPath.section].arrMachineHistory[indexPath.row].objArrDirective
        self.performSegue(withIdentifier: "toMachineDirective", sender: nil)
//            self.performSegue(withIdentifier: "toHistoryDetail", sender: nil)
    }
    
    //MARK: WebService Methods
    func callWebServiceForHistory() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ifFromMachine ? ["machineId" : machineID] : ["" : ""]
        let strUrl = ifFromMachine ? kBaseURL + NetworkConstant.Machine.getMachineOperationHistory : kBaseURL + NetworkConstant.Machine.getAllOperationHistory
        
        WebServiceHandler.postWebService(url: strUrl, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ParseMachineDetails.parseOperationHistoryList(response: response!, completionHandler: { (arrOperations) in
                        self.arrOperationHistory = arrOperations
                    })
                    
                    self.tblHistory.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toMachineDirective" {
            let vc = segue.destination as! MachineDirectiveViewController
            vc.isFromHistory = true
            vc.arrDirective = arrSelectedDirective
        }
    }

}
