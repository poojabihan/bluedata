//
//  ChangePasswordViewController.swift
//  BlueData
//
//  Created by kdstudio on 30/08/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    //MARK: IBOutlets
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnEyeCurrent: UIButton!
    @IBOutlet weak var btnEyeNew: UIButton!
    @IBOutlet weak var btnEyeConfirm: UIButton!

    //MARK: Variables
    let panel = JKNotificationPanel()

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtCurrentPassword.delegate = self
        txtNewPassword.delegate = self
        txtConfirmPassword.delegate = self
        
        txtCurrentPassword.rightView = btnEyeCurrent
        txtCurrentPassword.rightViewMode = .always

        txtNewPassword.rightView = btnEyeNew
        txtNewPassword.rightViewMode = .always

        txtConfirmPassword.rightView = btnEyeConfirm
        txtConfirmPassword.rightViewMode = .always

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Custom Methods
    func btnViewAction(_sender: UIButton) {
        switch _sender.tag {
        case 10:
            txtCurrentPassword.isSecureTextEntry = !txtCurrentPassword.isSecureTextEntry
        case 20:
            txtNewPassword.isSecureTextEntry = !txtNewPassword.isSecureTextEntry
        case 30:
            txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
        default:
            txtCurrentPassword.isSecureTextEntry = !txtCurrentPassword.isSecureTextEntry
        }
    }
    
    //MARK: UITextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Action Methods
    @IBAction func btnEyeAction(sender: UIButton) {
        switch sender.tag {
        case 10:
            txtCurrentPassword.isSecureTextEntry = !txtCurrentPassword.isSecureTextEntry
        case 20:
            txtNewPassword.isSecureTextEntry = !txtNewPassword.isSecureTextEntry
        case 30:
            txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
        default:
            return
        }
    }
    
    @IBAction func btnSaveAction() {
        
        if (txtCurrentPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter current pasword.")
        }
        else if (txtNewPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter new pasword.")
        }
        else if (txtConfirmPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter confirm pasword.")
        }
        else if txtNewPassword.text != txtConfirmPassword.text {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "New Password does not match the confirm password.")
        }
        else {
            callWebServiceForChangePassword()
        }
    }
    
    @IBAction func btnCrossAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: WebService Methods
    func callWebServiceForChangePassword() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["currentPassword" : txtCurrentPassword.text, "newPassword" : txtNewPassword.text]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.changePassword, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.btnCrossAction()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
