//
//  MachineDetailsViewController.swift
//  BlueData
//
//  Created by kdstudio on 12/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import Kingfisher

class MachineDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    //MARK: - IBOutlet
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var tblMachineDetails: UITableView!
    @IBOutlet weak var imgViewSwipe: UIImageView!
    @IBOutlet weak var btnLeftSwipe: UIButton!
    @IBOutlet weak var btnrightSwipe: UIButton!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var machineModel = MachineModel()
    var arrTitles = [String]()
    var arrSubTitles = [String]()
    var machineID = ""
    var currentImageIndex = 0
    var isRightSwipeEnable = true
    var isLeftSwipeEnable = false

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shadowView.dropShadow()
        tblMachineDetails.estimatedRowHeight = 65.0
        tblMachineDetails.tableFooterView = UIView()
        updateSwipeImages()
        tblMachineDetails.isHidden = true
        callWebServiceForMachineDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Action Methods
    @objc func btnMachineDirectiveAction() {
        print("btnMachineDirectiveAction Action")
        self.performSegue(withIdentifier: "toMachineDirectives", sender: nil)
    }
    
    @objc func btnUserDirectiveAction() {
        print("btnUserDirectiveAction Action")

        if machineModel.arrTrainedUsersIDs.contains(UserModel.sharedInstance.user_id){
            self.performSegue(withIdentifier: "toUserDirective", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "toTrainerPopUp", sender: nil)
        }
    }

    @objc func btnAddDocAction() {
        print("btnAddDocAction Action")
        self.performSegue(withIdentifier: "toAddDoc", sender: nil)
    }
    
    @objc func btnHistoryAction() {
        print("btnHistoryAction Action")
        self.performSegue(withIdentifier: "toHistory", sender: nil)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRightSwipeAction(_ sender: Any) {
        
        if isRightSwipeEnable {
            currentImageIndex = currentImageIndex + 1
            if machineModel.arrMachineImages.count - 1 == currentImageIndex {
                isRightSwipeEnable = false
            }
            if currentImageIndex > 0 {
                isLeftSwipeEnable = true
            }
            imgViewSwipe.kf.setImage(with: URL(string: machineModel.arrMachineImages[currentImageIndex]), placeholder: #imageLiteral(resourceName: "default_image"))
            updateSwipeImages()
        }
    }

    @IBAction func btnLeftSwipeAction(_ sender: Any) {

        if isLeftSwipeEnable {
            currentImageIndex = currentImageIndex - 1
            if currentImageIndex == 0 {
                isLeftSwipeEnable = false
                isRightSwipeEnable = true
            }
            else {
                isRightSwipeEnable = true
            }
            imgViewSwipe.kf.setImage(with: URL(string: machineModel.arrMachineImages[currentImageIndex]), placeholder: #imageLiteral(resourceName: "default_image"))
            updateSwipeImages()
        }
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        swipe.direction = .down
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(swipe)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
//        self.tabBarController?.tabBar.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCurlDown, animations: {
            sender.view?.alpha = 0
        }) { _ in
            sender.view?.removeFromSuperview()
        }

    }

    
    //MARK: UITableView Datasource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == arrTitles.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonFooterTableViewCell", for: indexPath) as! ButtonFooterTableViewCell
            cell.btnMachineDirective.addTarget(self, action: #selector(btnMachineDirectiveAction), for: .touchUpInside)
            cell.btnUserDirective.addTarget(self, action: #selector(btnUserDirectiveAction), for: .touchUpInside)
            cell.btnAddDoc.addTarget(self, action: #selector(btnAddDocAction), for: .touchUpInside)
            cell.btnHistory.addTarget(self, action: #selector(btnHistoryAction), for: .touchUpInside)

            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrTitles[indexPath.row]
            cell.lblSubTitle.text = arrSubTitles[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == arrTitles.count {
            return 180
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    //MARK: WebService Methods
    func callWebServiceForMachineDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["id" : machineID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Machine.machineDetails, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ParseMachineDetails.parseMachineDetails(response: response!, completionHandler: { (model) in
                        self.machineModel = model
                    })
                    
                    self.setupMachineData()
                    self.tblMachineDetails.isHidden = false
                    self.tblMachineDetails.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: Custom Methods
    func setupMachineData() {
        self.arrTitles = ["Organisation", "Office", "Machine Name", "Machine ID", "Machine Location", "Status", "Inductors"]
        self.arrSubTitles = [self.machineModel.strOrganisationName, self.machineModel.strOfficeName, self.machineModel.strMachineName, self.machineModel.strMachineID, self.machineModel.strLatitude + ", " + self.machineModel.strLongitude, self.machineModel.strMachineStatus, self.machineModel.strInductorsName]
        imgViewSwipe.kf.setImage(with: URL(string: machineModel.arrMachineImages[0]), placeholder: #imageLiteral(resourceName: "default_image"))
        isRightSwipeEnable = machineModel.arrMachineImages.count > 1 ?  true : false
        updateSwipeImages()
    }
    
    func updateSwipeImages() {
        
        isRightSwipeEnable ? btnrightSwipe.setImage(#imageLiteral(resourceName: "next"), for: .normal) : btnrightSwipe.setImage(#imageLiteral(resourceName: "next_disable"), for: .normal)
        isLeftSwipeEnable ? btnLeftSwipe.setImage(#imageLiteral(resourceName: "previous"), for: .normal) : btnLeftSwipe.setImage(#imageLiteral(resourceName: "previous_disable"), for: .normal)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toMachineDirectives" {
            let vc = segue.destination as? MachineDirectiveViewController
            vc?.machineID = machineID
        }
        if segue.identifier == "toUserDirective" {
            let vc = segue.destination as? UserDirectiveViewController
            vc?.machineID = machineID
        }
        if segue.identifier == "toHistory" {
            let vc = segue.destination as? HistoryViewController
            vc?.machineID = machineID
            vc?.ifFromMachine = true
        }
    }

}
