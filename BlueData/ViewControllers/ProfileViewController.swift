//
//  ProfileViewController.swift
//  BlueData
//
//  Created by kdstudio on 16/07/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: IBOutlets
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var footerView: UIView!

    //MARK: Variables
    let arrTitles = ["","Organisation", "Office", "Department", "E-mail","First Name", "Last Name", "Contact"]
    var arrValues = ["","", "", "", "", "", "", ""]
    var isEditable = false
    let panel = JKNotificationPanel()
    var selectedProfileImage = UIImage()
    var isNewImage = false
    var imagePicker = UIImagePickerController()

    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        imagePicker.delegate = self
        callWebServiceForUserProfileDetails()
        
        tblProfile.tableFooterView = changePasswordView

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setShadow()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Keyboard Methods
    @objc func keyboardWillShow(_ notification:Notification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            //self.view.frame.origin.y -= keyboardSize.height
            var userInfo = notification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)
            
            var contentInset:UIEdgeInsets = self.tblProfile.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.tblProfile.contentInset = contentInset
            
            //get indexpath
            let indexpath = IndexPath.init(row: 1, section: 0)
            self.tblProfile.scrollToRow(at: indexpath as IndexPath, at: UITableViewScrollPosition.top, animated: true)
        }
    }
    @objc func keyboardWillHide(_ notification:Notification) {
        
        if ((notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            let contentInset:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.tblProfile.contentInset = contentInset
        }
    }
    
    //MARK: Custom Methods
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: {
            self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .white
        })
    }
    
    //MARK: Action Methods
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @objc func btnEditAction(_ sender: UIButton) {
        isEditable = !isEditable
        
        let indexContactNo = IndexPath(row: 5, section: 0)
        let cellContact: EditableTableViewCell = self.tblProfile.cellForRow(at: indexContactNo) as! EditableTableViewCell
        cellContact.txtSubHeader.becomeFirstResponder()
        tblProfile.tableFooterView = footerView
        tblProfile.reloadData()
    }
    
    @objc func btnSelectImageAction(_ sender: UIButton) {
        if isEditable {
            let alert = UIAlertController(title: "Select Profile Picture", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        callWebServiceForUserUpdateProfileDetails()
    }
    
    @IBAction func btnChangePasswordAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toChangePassword", sender: nil)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        isEditable = !isEditable
        tblProfile.tableFooterView = changePasswordView
        if !isEditable {
            self.view.endEditing(true)
        }
        tblProfile.reloadData()
    }
    
    //MARK: UIImagePickerController Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            selectedProfileImage = pickedImage
            isNewImage = true
            tblProfile.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: UITableView Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            
            if isNewImage {
                cell.imgProfile.image = selectedProfileImage
            }
            else {
                cell.imgProfile.kf.setImage(with: URL(string: UserModel.sharedInstance.profileImage), placeholder: #imageLiteral(resourceName: "user_default"))
            }
            cell.imgCameraEdit.isHidden = isEditable ? false : true
            cell.lblUsername.text = UserModel.sharedInstance.name
            cell.lblEmail.text = UserModel.sharedInstance.email
            cell.btnEdit.isHidden = isEditable ? true : false
            cell.btnEdit.addTarget(self, action: #selector(btnEditAction(_:)), for: .touchUpInside)
            cell.btnImgView.addTarget(self, action: #selector(btnSelectImageAction(_:)), for: .touchUpInside)
            return cell
        }
        else if indexPath.row > 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditableTableViewCell", for: indexPath) as! EditableTableViewCell
            cell.lblHeader.text = arrTitles[indexPath.row]
            cell.txtSubHeader.text = arrValues[indexPath.row]
            cell.txtSubHeader.isUserInteractionEnabled = isEditable ? true : false
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
            cell.lblTitle.text = arrTitles[indexPath.row]
            cell.lblSubTitle.text = arrValues[indexPath.row]
            cell.lblSubTitle.textColor = isEditable ? UIColor.lightGray : UIColor.black
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 270
        }
        else {
            return 65
        }
    }
    
    //MARK: WebService Methods
    func callWebServiceForUserProfileDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
//        let param = ["":""]
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: nil, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ParseMachineDetails.parseUserDetails(json: response!, completionHandler: { (model) in
                        self.arrValues = ["", model.organisation, model.office, model.department, model.email, model.fName, model.lName, model.phoneNo]
                    })
                    
                    self.tblProfile.reloadData()
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUserUpdateProfileDetails() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let indexContactNo = IndexPath(row: 7, section: 0)
        let cellContact: EditableTableViewCell = self.tblProfile.cellForRow(at: indexContactNo) as! EditableTableViewCell
        let contNo = cellContact.txtSubHeader.text!
        
        let indexFN = IndexPath(row: 5, section: 0)
        let cellFN: EditableTableViewCell = self.tblProfile.cellForRow(at: indexFN) as! EditableTableViewCell
        let fname = cellFN.txtSubHeader.text!
        
        let indexLN = IndexPath(row: 6, section: 0)
        let cellLN: EditableTableViewCell = self.tblProfile.cellForRow(at: indexLN) as! EditableTableViewCell
        let lname = cellLN.txtSubHeader.text!
        
        let param = ["contact": contNo,
                     "firstName":fname,
                     "lastName":lname,
                     "profileImage": isNewImage ? convertImageToBase64(image: selectedProfileImage) : ""]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateProfile, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    
                    ParseMachineDetails.parseUserDetails(json: response!, completionHandler: { (model) in
                        self.arrValues = ["", UserModel.sharedInstance.organisation, UserModel.sharedInstance.office, UserModel.sharedInstance.department, response!["data"]["email"].stringValue, response!["data"]["firstName"].stringValue, response!["data"]["lastName"].stringValue, response!["data"]["userContact"].stringValue]
                        self.tblProfile.reloadData()
                        self.btnCancelAction(UIButton())
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshSideBar"), object: nil)
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // convert images into base64 and keep them into string
    func convertImageToBase64(image: UIImage) -> String {
        
        var imgData = UIImagePNGRepresentation(image)!
        if let imageData = image.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
